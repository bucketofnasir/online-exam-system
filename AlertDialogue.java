package application;

import java.util.Optional;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class AlertDialogue {
	
	public static void Error(String errorTitle, String Errormsg){
		Alert alertDialogue = new Alert(AlertType.ERROR);
		alertDialogue.setTitle(errorTitle);
		alertDialogue.setHeaderText(null);
		alertDialogue.setContentText(Errormsg);
		alertDialogue.showAndWait();
	}
	public static void Info(String Infomsg){
		Alert alert= new Alert(AlertType.INFORMATION);
		alert.setTitle("Confirm question");
		alert.setContentText(Infomsg);
		alert.showAndWait();
	}
	public static void Warning(String warningTitle, String Warningmsg){
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle(warningTitle);
		alert.setHeaderText(null);
		alert.setContentText(Warningmsg);
		alert.showAndWait();
	}
	public static ButtonType showConfirmationAlertBox(String message)
	{
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmation Dialoge");
		alert.setHeaderText(null);
		alert.setContentText(message);
		Optional<ButtonType> action = alert.showAndWait();
		return action.get();
	}
	public static void closeDialogStage(Button Btn)
	{
		Stage stage = (Stage) Btn.getScene().getWindow();
		stage.close();
	}
	
}
