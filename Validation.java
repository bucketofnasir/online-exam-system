package application;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validation {
	public static boolean EmailValidation(String email){
		Pattern p= Pattern.compile("[a-zA-Z0-9][a-zA-Z0-9._]*@[a-zA-Z0-9]+([.][a-zA-Z]+)+");
		Matcher m= p.matcher(email);
		if (m.find()&&m.group().equals(email)) {
			return true;
		}
		else {
			return false;
		}
	}
	public static boolean IDValidation(String ID){
		Pattern p= Pattern.compile("[0-9][0-9._]+");
		Matcher m= p.matcher(ID);
		if (m.find()&&m.group().equals(ID)) {
			return true;
		}
		else {
			return false;
		}
	}
}
