package application;
	
import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;


public class Main extends Application {
	private static Stage stage;
	private static BorderPane mainLayout;
	@Override
	public void start(Stage primaryStage) throws IOException {
		Main.stage=primaryStage;
		Main.stage.setTitle("My Application");
		Main.showLoginPage();
	}
	public static void showLoginPage() throws IOException{
		FXMLLoader loader= new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/LoginPage.fxml"));
		mainLayout=loader.load();
		Scene scene= new Scene(mainLayout);
		stage.setScene(scene);
		stage.show();
	}
	public static void showRegistrationPage() throws IOException{
		FXMLLoader loader= new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/RegistrationPage.fxml"));
		mainLayout=loader.load();
		Scene scene= new Scene(mainLayout);
		stage.setScene(scene);
		stage.show();
	}
	public static void showAdminHomePage() throws IOException{
		FXMLLoader loader= new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/AdminHomePage.fxml"));
		mainLayout=loader.load();
		Scene scene= new Scene(mainLayout);
		stage.setScene(scene);
		stage.show();
	}
	public static void showFacultyHomePage() throws IOException{
		FXMLLoader loader= new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/FacultyHomePage.fxml"));
		mainLayout=loader.load();
		Scene scene= new Scene(mainLayout);
		stage.setScene(scene);
		stage.show();
	}
	public static void showStudentHomePage() throws IOException{
		FXMLLoader loader= new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/StudentHomePage.fxml"));
		mainLayout=loader.load();
		Scene scene= new Scene(mainLayout);
		stage.setScene(scene);
		stage.show();
	}
	public static void showExam() throws IOException{
		FXMLLoader loader= new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/Exam.fxml"));
		mainLayout=loader.load();
		Scene scene= new Scene(mainLayout);
		stage.setScene(scene);
		stage.show();
	}
	public static void showQuestions() throws IOException{
		FXMLLoader loader= new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/Questions.fxml"));
		mainLayout=loader.load();
		Scene scene= new Scene(mainLayout);
		stage.setScene(scene);
		stage.show();
	}
	public static void showResult() throws IOException{
		FXMLLoader loader= new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/Result.fxml"));
		mainLayout=loader.load();
		Scene scene= new Scene(mainLayout);
		stage.setScene(scene);
		stage.show();
	}
	public static void showUpdateProfile() throws IOException{
		FXMLLoader loader= new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/UpdateProfile.fxml"));
		mainLayout=loader.load();
		Scene scene= new Scene(mainLayout);
		stage.setScene(scene);
		stage.show();
	}
	public static void showFacultyUpdateProfile() throws IOException{
		FXMLLoader loader= new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/UpdateFacultyProfile.fxml"));
		mainLayout=loader.load();
		Scene scene= new Scene(mainLayout);
		stage.setScene(scene);
		stage.show();
	}
	public static void main(String[] args) {
		launch(args);
	}
}
