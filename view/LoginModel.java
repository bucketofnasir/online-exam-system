package application.view;

import java.sql.*;

public class LoginModel
{
	public static String Status;
	static Connection connection;
	public LoginModel()
	{
		connection=DBConnection.DBConnector();
		if(connection==null)
		{
			System.exit(1);
		}
	}
	public static boolean isLoginValidForAdmin(String user, String pass, String Admin ) throws SQLException 
	{
		PreparedStatement preparedStatement=null;
		ResultSet resultSet=null;
		String query="select * from UserInfo where Name=? and password=? and UserRoll=?";
		
		try
		{
			preparedStatement =connection.prepareStatement(query);
			preparedStatement.setString(1, user);
			preparedStatement.setString(2, pass);
			preparedStatement.setString(3, Admin);
			
			resultSet=preparedStatement.executeQuery();
			
			if(resultSet.next())
				return true;
			else
				return false;
		}
		catch(Exception e)
		{
			return false;
		}
		finally
		{
			preparedStatement.close();
			resultSet.close();
		}
	}	
	public static boolean isLoginValidForStudent(String ID, String pass, String student, String status) throws SQLException
	{
		PreparedStatement preparedStatement=null;
		ResultSet resultSet=null;
		String query="select * from UserInfo where ID=? and password=? and UserRoll=? and Status=?";
		
		try
		{
			preparedStatement =connection.prepareStatement(query);
			preparedStatement.setString(1, ID);
			preparedStatement.setString(2, pass);
			preparedStatement.setString(3, student);
			preparedStatement.setString(4, status);
			resultSet=preparedStatement.executeQuery();
			
			if(resultSet.next())
				return true;
			else
				return false;
		}
		catch(Exception e)
		{
			return false;
		}
		finally
		{
			preparedStatement.close();
			resultSet.close();
		}
	}
	public static boolean isLoginValidForFaculty(String email, String pass, String faculty, String status) throws SQLException
	{
		PreparedStatement preparedStatement=null;
		ResultSet resultSet=null;
		String query="select * from UserInfo where Email=? and Password=? and UserRoll=? and Status=?";
		
		try
		{
			preparedStatement =connection.prepareStatement(query);
			preparedStatement.setString(1, email);
			preparedStatement.setString(2, pass);
			preparedStatement.setString(3, faculty);
			preparedStatement.setString(4, status);
			
			resultSet=preparedStatement.executeQuery();
			
			if(resultSet.next())
				return true;
			else
				return false;
		}
		catch(Exception e)
		{
			return false;
		}
		finally
		{
			preparedStatement.close();
			resultSet.close();
		}
	}
}
