package application.view;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {
	public static Connection DBConnector()
	{
		try
		{
//			Class.forName("com.mysql.jdbc.Driver");
			Class.forName("org.sqlite.JDBC");
			
//			Connection conn=DriverManager.getConnection("jdbc:sqlite:D:\\java.sqlite");
			Connection conn=DriverManager.getConnection("jdbc:sqlite:E:\\Java\\JavaFX\\src\\application\\database\\java.sqlite");
//			Connection conn=DriverManager.getConnection(  "jdbc:mysql://localhost:3306/java","root","");  
			return conn;
		}
		catch(Exception e)
		{
			System.out.println(e);
			return null;
		}
	}
}
