package application.view;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.io.IOException;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

import application.AlertDialogue;
import application.Main;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.util.Duration;
import javafx.scene.control.Label;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ExamController implements Initializable{
	Connection connection= DBConnection.DBConnector();
	PreparedStatement statement=null;
	ResultSet rs=null;
	
	cor corAnswer= new cor();
	
	private Timeline timeline;
	 private DoubleProperty timeSeconds = new SimpleDoubleProperty();
	 private Duration time = Duration.seconds(0);


	@FXML
	public Label TimeTracker;
	@FXML
	private Label Sequence;
	@FXML
	private RadioButton Option1;
	@FXML
	private RadioButton Option3;
	@FXML
	private RadioButton Option4;
	@FXML
	private RadioButton Option2;
	
	@FXML
	private Button SubmitButton;
	@FXML
	private Button EndButton;
	@FXML
	private Label Question;
	@FXML
	private Button CloseButton;
	
	
	public static javax.swing.Timer timer;
	
	private int qSequence=0;		//serialized question
	private int randQuestion;		//load question from database randomly
	public static String corrAns;
	int incor=0;
	int cor=0;
	public static String subject;
	
	
	
	@FXML
	public void goSubmit(ActionEvent event) throws SQLException{
		if (qSequence!=10) {
			
			qSequence=qSequence+1;
			Sequence.setText(Integer.toString(qSequence));
			if (Option1.isSelected()==true) {
				if (Option1.getText().equals(corAnswer.getCorAns())==true) {
					cor=cor+1;
					corAnswer.setcor(cor);
				}
				else {
					incor=incor+1;
					corAnswer.setinc(incor);
				}
			}else if (Option2.isSelected()==true) {
				if (Option2.getText().equals(corAnswer.getCorAns())==true) {
					cor=cor+1;
					corAnswer.setcor(cor);
				}
				else {
					incor=incor+1;
					corAnswer.setinc(incor);
				}
			}else if (Option3.isSelected()==true) {
				if (Option3.getText().equals(corAnswer.getCorAns())==true) {
					cor=cor+1;
					corAnswer.setcor(cor);
				}
				else {
					incor=incor+1;
					corAnswer.setinc(incor);
				}
			}else if (Option4.isSelected()==true) {
				if (Option4.getText().equals(corAnswer.getCorAns())==true) {
					cor=cor+1;
					corAnswer.setcor(cor);
				}
				else {
					incor=incor+1;
					corAnswer.setinc(incor);
				}
			}
			else {
				incor=incor+1;
				corAnswer.setinc(incor);
			}
			
		}else {
			AlertDialogue.Info("Your Exam is over. Wait for result!");
			
		}
//		AlertDialogue.Info("Correct: "+corAnswer.getcor()+" Incorrect: "+corAnswer.getinc()+"ans: "+corAnswer.getCorAns());
		if (corAnswer.getcor()>=5&&corAnswer.getcor()<=7) {
			corAnswer.setStatus("Passed");
			corAnswer.setremark("Good");
		}
		else if (corAnswer.getcor()>=8&&corAnswer.getcor()<=10) {
			corAnswer.setStatus("Passed");
			corAnswer.setremark("Excellent");
		}
		else {
			corAnswer.setStatus("Failed");
			corAnswer.setremark("Bad");
		}
		
		Random rn=new Random();
		randQuestion=rn.nextInt(23)+1;
		try {
			loadQuestion(Integer.toString(randQuestion), subject);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@FXML
	public void goEnd(ActionEvent event) throws IOException, SQLException {
		
		String corrAns=Integer.toString(corAnswer.getcor());
		String incorrAns=Integer.toString(corAnswer.getinc());
		String Status=corAnswer.getStatus();
		String insertQuery="INSERT INTO Result(ID,Subject, Correct, Incorrect, Status, Remark) VALUES(?, ?, ?, ?, ?, ?)";
		try {
			statement=connection.prepareStatement(insertQuery);
			statement.setString(1, StudentHomePageController.ID);
			statement.setString(2, subject);
			statement.setString(3, corrAns);
			statement.setString(4, incorrAns);
			statement.setString(5, Status);
			statement.setString(6, corAnswer.getremark());
			rs=statement.executeQuery();
			
		} catch (Exception e) {
			System.out.println(e);
		}finally {
			statement.execute();
			statement.close();
		}
		Main.showStudentHomePage();
	}
	@FXML
	public void goClose() throws SQLException{
		
		String corrAns=Integer.toString(corAnswer.getcor());
		String incorrAns=Integer.toString(corAnswer.getinc());
		String query="INSERT INTO Result(ID, Subject, Correct, Incorrect, Status, Remark) VALUES(?, ?, ?, ?, ?, ?)";
		try {
			statement=connection.prepareStatement(query);
			statement.setString(1, StudentHomePageController.ID);
			statement.setString(2, subject);
			statement.setString(3, corrAns);
			statement.setString(4, incorrAns);
			statement.setString(5, corAnswer.getStatus());
			statement.setString(6, corAnswer.getremark());
			rs=statement.executeQuery();
			
		} catch (Exception e) {
			System.out.println(e);
		}finally {
			statement.execute();
			statement.close();
		}
		System.exit(0);
	}
	@Override
	public void initialize(URL args0, ResourceBundle args1) {
		Sequence.setText("1");
		Random rn= new Random();
		int i=rn.nextInt(23);
		String rnsq=Integer.toString(i);
		
		try {
			loadQuestion(rnsq,subject);
			showTime();
		} catch (SQLException e) {
			AlertDialogue.Error("Question loading error","Question can't be loaded. Try again");
		}
	}
	public void loadQuestion(String i, String subject) throws SQLException{
		String query="SELECT * FROM Questions WHERE Serial=? and Subject=?";
		statement=connection.prepareStatement(query);
		statement.setString(1, i);
		statement.setString(2, subject);
		rs=statement.executeQuery();
		while (rs.next()) {
			Question.setText(rs.getString("question"));
			Option1.setText(rs.getString("option1"));
			Option2.setText(rs.getString("option2"));
			Option3.setText(rs.getString("option3"));
			Option4.setText(rs.getString("option4"));
			
			
			corAnswer.setCorAns(rs.getString("Answer"));
		}
		statement.close();
		rs.close();
	}
	
	
	public void showTime(){
		TimeTracker.textProperty().bind(timeSeconds.asString());
    	if (TimeTracker.getText()=="5.0") {
			TimeTracker.setText("0.0");
		}
    	else {
    		if (timeline != null) {
            } else {
                timeline = new Timeline(
                    new KeyFrame(Duration.millis(100),
                    new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent t) {
                            Duration duration = ((KeyFrame)t.getSource()).getTime();
                            time = time.add(duration);
                            timeSeconds.set(time.toSeconds());
                        }
                    })
                );
                
            }
    		timeline.setCycleCount(Animation.INDEFINITE);
            timeline.play();
		}
    }
}

