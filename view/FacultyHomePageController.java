package application.view;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.function.Predicate;

import application.AlertDialogue;
import application.Main;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;

import javafx.scene.control.Label;

import javafx.scene.control.MenuItem;

import javafx.scene.control.ComboBox;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class FacultyHomePageController implements Initializable{
	
	/*******************************************/
	/********Getting Connection from DBConnect**/
	/*******************************************/
	Connection connection= DBConnection.DBConnector();
	PreparedStatement statement=null;
	ResultSet rs=null;
	
	/********************************************/
	/********Adding And Searching Property*******/
	/********************************************/
	ObservableList<QuestionLoader> Data= FXCollections.observableArrayList();
	ObservableList<SubjectLoader> SubjectData= FXCollections.observableArrayList();
	
	/************************************************/
	/*********properties of update question section**/
	/************************************************/
	public static String questionName;
	public static String option1;
	public static String option2;
	public static String option3;
	public static String option4;
	public static String Ans;
	public static String Desc;
	public static String Author;
	
	/****************************************/
	/********Search box property*************/
	/****************************************/
	FilteredList<QuestionLoader> filteredData=new FilteredList<>(Data,e->true);
	FilteredList<SubjectLoader> subjectFilter= new FilteredList<>(SubjectData,e->true);
	
	
	/****************************************/
	/**********GUI properties****************/
	/****************************************/
	@FXML
	private ImageView imageView;
	@FXML
	private Button UploadImage;
	@FXML
	private Button LogOut;
	@FXML
	private Label Name;
	@FXML
	private Label Email;
	@FXML
	private Label DateofBirth;
	@FXML
	private Label PhoneNumber;
	@FXML
	private Button updateQuestion;
	@FXML
	private Button DeleteQuestion;
	@FXML
	private TextField SearchQuestion;
	@FXML
	private ComboBox<String> ComboBox;
	@FXML
	private TextField Question;
	@FXML
	private TextField Option1;
	@FXML
	private TextField Option2;
	@FXML
	private TextField Option3;
	@FXML
	private TextField Option4;
	@FXML
	private TextField Answer;
	@FXML
	private TextField Description;
	@FXML
	private Button AddQuestion;
	@FXML
	private Button UpdateSubject;
	@FXML
	private Button DeleteSubject;
	@FXML
	private TextField SearchSubject;
	@FXML
	private TextField SubjectField;
	@FXML
	private Button AddSubject;
	@FXML
	private MenuItem Close;
	@FXML
	private MenuItem UpdateProfile;
	@FXML
	private MenuItem About;
	@FXML
	private MenuItem DeleteFacultyAccount;
	
	
	/****************************************/
	/********Loading Question property*******/
	/****************************************/
	
	@FXML
	private TableView<QuestionLoader> Table;
	@FXML
	private TableColumn<?,?> QuestionCol;
	@FXML
	private TableColumn<?,?> Option1Col;
	@FXML
	private TableColumn<?,?> Option2Col;
	@FXML
	private TableColumn<?,?> Option3Col;
	@FXML
	private TableColumn<?,?> Option4Col;
	@FXML
	private TableColumn<?,?> AnswerCol;
	@FXML
	private TableColumn<?,?> DescriptionCol;
	@FXML
	private TableColumn<?,?> AuthorCol;
	@FXML
	private TableColumn<?,?> SubjectCol;
	
	
	/**********************************************/
	/**********Loading Subject Properties**********/
	/**********************************************/
	
	@FXML
	private TableView<SubjectLoader> SubjectTable;
	@FXML
	private TableColumn<?,?> singleSubjectCol;
	@FXML
	private TableColumn<?,?> TotalQuestionCol;
	
	ObservableList<String> SubjectList= FXCollections.observableArrayList(
			SubjectList()
			);
	boolean insert_rs;
	
	
	/*******************************************/
	/**********Custom Variables*****************/
	/*******************************************/
	public static String facultyEmail;
	public static String ID;
	public static String name;
	public static String password;
	public static String departent;
	public static String gender;
	public static String address;
	public static String phonenumber;
	public static String dateofbirth;
	public static String userRole;
	
	/********************************************/
	/*********All Method is started from here****/
	/********************************************/
	@FXML
	public void goLogOut(ActionEvent event) throws IOException {
		Main.showLoginPage();
	}
	@FXML
	public void goUpdateQuestion(ActionEvent event) throws IOException, SQLException {
		try{
			QuestionLoader question =(QuestionLoader)Table.getSelectionModel().getSelectedItem();
				if (question!= null) {
					String query = "select * from Questions where question=?";
					statement = connection.prepareStatement(query);
					statement.setString(1, question.getQuestion());
					rs=statement.executeQuery();
						questionName=rs.getString("question");
						option1=rs.getString("option1");
						option2=rs.getString("option2");
						option3=rs.getString("option3");
						option4=rs.getString("option4");
						Ans=rs.getString("Answer");
						Desc= rs.getString("Description");
					statement.close();
					rs.close();
					Main.showFacultyHomePage();
					showUpdateQuestionbyFaculty();
				}else {
					AlertDialogue.Warning("Select question warning","Select A question!");
				}	
			}
		catch (Exception e){
			System.err.println(e);
		}
	}
	@FXML
	public void goUploadImage(){
//		FileChooser fc= new FileChooser();
//		File selectedfile= fc.showOpenDialog(null);
//		if (selectedfile!=null) {
//			image=new Image(getClass().getResourceAsStream(selectedfile.getAbsolutePath()), 100,150, true, true);
//			imageView= new ImageView(image);
//			imageView.setPreserveRatio(true);
//		}
	}
	@FXML
	public void goDeleteQuestion(ActionEvent event) {
		ButtonType buttonType=AlertDialogue.showConfirmationAlertBox("Do you want to delete?");
		if (buttonType != ButtonType.OK)
			return;
			String name;
			try{
				QuestionLoader question = (QuestionLoader) Table.getSelectionModel().getSelectedItem();
				if (question!=null) {
					String query = "delete from Questions where question=?";
					statement = connection.prepareStatement(query);
					statement.setString(1, question.getQuestion());
					name = question.getQuestion();
					statement.executeUpdate();
					statement.close();
					rs.close();
					Main.showFacultyHomePage();
					AlertDialogue.Info("Question '" + name + "' Deleted Successfully!");
				}
				else {
					AlertDialogue.Error("Empty Selection","No question is selected!");
				}
			}
			catch (Exception e)
			{
				System.err.println(e);
			}
	}
	@FXML
	public void goDeleteSubject(){
		ButtonType buttonType=AlertDialogue.showConfirmationAlertBox("Do you want to delete?");
		if (buttonType != ButtonType.OK)
			return;
			String name;
			try{
				SubjectLoader subject = (SubjectLoader) SubjectTable.getSelectionModel().getSelectedItem();
				if (subject!=null) {
					String query = "delete from Subject where AllSubject=?";
					statement = connection.prepareStatement(query);
					statement.setString(1, subject.getAllSubject());
					name = subject.getAllSubject();
					statement.executeUpdate();
					statement.close();
					rs.close();
					Main.showFacultyHomePage();
					AlertDialogue.Info("Subject '" + name + "' Deleted Successfully!");
				}
				else {
					AlertDialogue.Error("Empty Selection","No subject is selected!");
				}
			}
			catch (Exception e)
			{
				System.err.println(e);
			}
		
	}
	@FXML
	public void goSearchQuestion(ActionEvent event) {
		SearchQuestion.textProperty().addListener((observableValue,oldValue,newValue)->{
			filteredData.setPredicate((Predicate<? super QuestionLoader>)question->{
				if(newValue==null||newValue.isEmpty()){
					return true;
				}
				String lowerCaseFilter=newValue.toLowerCase();
				if(question.getQuestion().toLowerCase().contains(lowerCaseFilter)){
					return true;
				}
				return false;
			});
		});
		SortedList<QuestionLoader> sortedData=new SortedList<>(filteredData);
		sortedData.comparatorProperty().bind(Table.comparatorProperty());
		Table.setItems(sortedData);
	}
	
	@FXML
	public void goAddQuestion(ActionEvent event) throws SQLException {

		String question= Question.getText();
		String op1= Option1.getText();
		String op2= Option2.getText();
		String op3= Option3.getText();
		String op4= Option4.getText();
		String ans= Answer.getText();
		String shortdesc= Description.getText();
		String subject=ComboBox.getValue();
		String Author=facultyEmail;
		if ((question.isEmpty()||op1.isEmpty()||op2.isEmpty()||op3.isEmpty()||op4.isEmpty()||ans.isEmpty())==false) {
			String query= "INSERT INTO Questions(question, option1, option2, option3, option4, Answer, Description, Subject, Author) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
			statement=connection.prepareStatement(query);
			statement.setString(1, question);
			statement.setString(2, op1);
			statement.setString(3, op2);
			statement.setString(4, op3);
			statement.setString(5, op4);
			statement.setString(6, ans);
			statement.setString(7, shortdesc);
			statement.setString(8, subject);
			statement.setString(9, Author);
			insert_rs= statement.execute();
			if (insert_rs==false) {
				AlertDialogue.Info("Question is Added Successfully");
				resetField();
				
			}
			else {
				AlertDialogue.Error("Adding Error","question is not added successfully!");
				resetField();
			}
			statement.close();
		}else {
			Alert alert= new Alert(AlertType.ERROR);
			alert.setTitle("Error Question");
			alert.setContentText("Field can not be empty!");
			alert.showAndWait();
			resetField();
		}
		loadQuestion();
	}
	
	@FXML
	public void goSearchSubject(ActionEvent event) {
		SearchSubject.textProperty().addListener((observableValue,oldValue,newValue)->{
			subjectFilter.setPredicate((Predicate<? super SubjectLoader>)subject->{
				if(newValue==null||newValue.isEmpty()){
					return true;
				}
				String lowerCaseFilter=newValue.toLowerCase();
				if(subject.AllSubject.toLowerCase().contains(lowerCaseFilter)){
					return true;
				}
				return false;
			});
		});
		SortedList<SubjectLoader> sortedData=new SortedList<>(subjectFilter);
		sortedData.comparatorProperty().bind(SubjectTable.comparatorProperty());
		SubjectTable.setItems(sortedData);
	}
	
	@FXML
	public void goAddSubject(ActionEvent event) throws SQLException {

		String SubjectName= SubjectField.getText();
		
		if ((SubjectName.isEmpty())==false) {
			String query= "INSERT INTO Subject(AllSubject) VALUES(?)";
			statement=connection.prepareStatement(query);
			statement.setString(1, SubjectName);
			insert_rs= statement.execute();
			if (insert_rs==false) {
				AlertDialogue.Info("Subject is Added Successfully");
				resetField();
				
			}
			else {
				AlertDialogue.Error("Adding Error","Subject is not added successfully!");
				resetField();
			}
			statement.close();
		}else {
			Alert alert= new Alert(AlertType.ERROR);
			alert.setTitle("Error Question");
			alert.setContentText("Field can not be empty!");
			alert.showAndWait();
			resetField();
		}
		loadSubject();
	}
	
	@FXML
	public void goClose(ActionEvent event) {
		System.exit(0);
	}
	
	@FXML
	public void goUpdateProfile(ActionEvent event) throws IOException {

		String query="SELECT * FROM UserInfo WHERE Email='"+facultyEmail+"'";
		try {
			statement=connection.prepareStatement(query);
			rs=statement.executeQuery();
			while(rs.next()){
				ID=rs.getString("ID");
				name=rs.getString("Name");
				facultyEmail=rs.getString("Email");
				departent=rs.getString("Department");
				gender=rs.getString("Gender");
				dateofbirth=rs.getString("BirthDate");
				address=rs.getString("Address");
				phonenumber=rs.getString("PhoneNumber");
				password=rs.getString("Password");
				userRole=rs.getString("UserRoll");
			}
			Main.showFacultyUpdateProfile();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	@FXML
	public void goDeleteFacultyAccount() throws SQLException, IOException{
		String query="delete from UserInfo WHERE Email='"+facultyEmail+"'";
		statement=connection.prepareStatement(query);
		boolean rs=statement.execute();
		if (rs==true) {
			AlertDialogue.Info("Account is deleted successfully!");
		}
		else {
			AlertDialogue.Error("Deleting error", "Account can't be deleted!");
		}
		Main.showLoginPage();
	}
	
	@FXML
	public void goAbout(ActionEvent event) {
		
	}
	
	@Override
	public void initialize(URL args0, ResourceBundle args1) {
		ComboBox.setPromptText("Select a Subject");
		ComboBox.setItems(SubjectList);
		QuestionCol.setCellValueFactory(new PropertyValueFactory<>("question"));
		Option1Col.setCellValueFactory(new PropertyValueFactory<>("option1"));
		Option2Col.setCellValueFactory(new PropertyValueFactory<>("option2"));
		Option3Col.setCellValueFactory(new PropertyValueFactory<>("option3"));
		Option4Col.setCellValueFactory(new PropertyValueFactory<>("option4"));
		AnswerCol.setCellValueFactory(new PropertyValueFactory<>("Answer"));
		SubjectCol.setCellValueFactory(new PropertyValueFactory<>("Subject"));
		DescriptionCol.setCellValueFactory(new PropertyValueFactory<>("Description"));
		AuthorCol.setCellValueFactory(new PropertyValueFactory<>("Author"));
		singleSubjectCol.setCellValueFactory(new PropertyValueFactory<>("AllSubject"));
		TotalQuestionCol.setCellValueFactory(new PropertyValueFactory<>("TotalQuestion"));
		loadQuestion();
		loadSubject();
		try {
			loadProfile();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void loadQuestion(){
		String query="SELECT * FROM Questions";
		try {
			Data.clear();
			statement=connection.prepareStatement(query);
			rs=statement.executeQuery();
			
			while (rs.next()) {
				Data.addAll(new QuestionLoader(
						rs.getString("question"), 
						rs.getString("option1"),
						rs.getString("option2"), 
						rs.getString("option3"), 
						rs.getString("option4"), 
						rs.getString("Answer"), 
						rs.getString("Subject"),
						rs.getString("Author"),
						rs.getString("Description")
						));
				Table.setItems(Data);
			}
			statement.close();
			rs.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	public void loadSubject(){
		String query="SELECT * FROM Subject";
		try {
			SubjectData.clear();
			statement=connection.prepareStatement(query);
			rs=statement.executeQuery();
			
			while (rs.next()) {
				SubjectData.addAll(new SubjectLoader(
						rs.getString("AllSubject"),
						rs.getString("TotalQuestion")
						));
				SubjectTable.setItems(SubjectData);
			}
			statement.close();
			rs.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	public ArrayList<String> SubjectList(){
		ArrayList<String>list = new ArrayList<>();
		String query="SELECT * from Subject";
		try {
			statement=connection.prepareStatement(query);
			rs=statement.executeQuery();
			while (rs.next()) {
				list.add(rs.getString("AllSubject"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	/******************************/
	/*********Reset All Field******/
	/******************************/
	public void resetField(){
		Question.setText("");
		Option1.setText("");
		Option2.setText("");
		Option3.setText("");
		Option4.setText("");
		Answer.setText("");
		SubjectField.setText("");
		
	}
	
	public void loadProfile() throws SQLException{
		String query="SELECT * FROM UserInfo WHERE Email=?";
		statement=connection.prepareStatement(query);
		statement.setString(1, facultyEmail);
		rs=statement.executeQuery();
		while(rs.next()){
			Name.setText(rs.getString("Name"));
			Email.setText(rs.getString("Email"));
			PhoneNumber.setText(rs.getString("PhoneNumber"));
			DateofBirth.setText(rs.getString("BirthDate"));
		}
	}
	public void showUpdateQuestionbyFaculty() throws IOException{

		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/UpdateQuestionbyFaculty.fxml"));
		BorderPane UserInfoBorderPane = loader.load();

		Stage addDialogStage = new Stage();
		addDialogStage.setResizable(false);
		addDialogStage.setTitle("Update Question");
		addDialogStage.initModality(Modality.WINDOW_MODAL);
//		addDialogStage.initOwner(primaryStage);
		Scene scene = new Scene(UserInfoBorderPane);
		addDialogStage.setScene(scene);
		addDialogStage.showAndWait();
	}
}