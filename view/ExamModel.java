package application.view;

import java.sql.*;

public class ExamModel
{
	static Connection connection;
	public ExamModel()
	{
		connection=DBConnection.DBConnector();
		if(connection==null)
		{
			System.exit(1);
		}
	}
	
	public static boolean isQuestionValid(String question, String option1, String option2, String option3, String option4 ) throws SQLException 
	{
		PreparedStatement preparedStatement=null;
		ResultSet resultSet=null;
		String query="select * from Questions where question=? and option1=? and option2=? and option3=? and option4=?";
		
		try
		{
			preparedStatement =connection.prepareStatement(query);
			preparedStatement.setString(1, question);
			preparedStatement.setString(2, option1);
			preparedStatement.setString(3, option2);
			preparedStatement.setString(4, option3);
			preparedStatement.setString(5, option4);
			
			resultSet=preparedStatement.executeQuery();
			
			if(resultSet.next())
				return true;
			else
				return false;
		}
		catch(Exception e)
		{
			return false;
		}
		finally
		{
			preparedStatement.close();
			resultSet.close();
		}
	}	
}
