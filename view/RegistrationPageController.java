package application.view;

import javafx.fxml.FXML;

import javafx.scene.control.Button;

import javafx.scene.control.TextField;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import application.AlertDialogue;
import application.Main;
import application.Validation;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

import javafx.scene.control.ComboBox;

import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.DatePicker;

public class RegistrationPageController {
	
	Connection connection=DBConnection.DBConnector();
	PreparedStatement statement = null;
	ResultSet resultSet=null;
	ObservableList<String> departmentlist= FXCollections.observableArrayList(
			"CSE","EEE","BBA"
			);
	
	public static String userRole;
	
	@FXML
	private RadioButton Male;
	@FXML
	private RadioButton Female;
	@FXML
	private TextField FullName;
	@FXML
	private TextField Email;
	@FXML
	private TextField ID;
	@FXML
	private PasswordField Password;
	@FXML
	private ComboBox<String> Department;
	@FXML
	private TextField Address;
	@FXML
	private DatePicker DateofBirth;
	@FXML
	private TextField phoneNumber;
	@FXML
	private Button BackButton;
	@FXML
	private Button RegisterButton;
	@FXML
	private Button ExitButton;
	@FXML
	public void goBack(ActionEvent event) throws IOException {
		Main.showLoginPage();
	}
	
	
	@FXML
	public void goRegister() throws SQLException, IOException{
		String userStatus="Enable";
		String gender;
		if (Male.isSelected()==true) {
			gender="Male";
		}
		else {
			gender="Female";
		}
		String name=FullName.getText();
		String email=Email.getText();
		String password=Password.getText();
		String address=Address.getText();
		String department=Department.getValue();
		String phonenumber=phoneNumber.getText();
		LocalDate birthdate=DateofBirth.getValue();
		String id=ID.getText();
		String query="INSERT INTO UserInfo (Name, Email, ID, Password, Department, Address, Phonenumber, UserRoll, BirthDate, Gender, Status) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		if ((name.isEmpty()||email.isEmpty()||password.isEmpty())==false) {
			if (Validation.EmailValidation(email)==true) {
				try
				{
					statement =connection.prepareStatement(query);
					statement.setString(1, name);
					statement.setString(2, email);
					statement.setString(3, id);
					statement.setString(4, password);
					statement.setString(5, department);
					statement.setString(6, address);
					statement.setString(7, phonenumber);
					statement.setString(8, userRole);
					statement.setString(9, birthdate.toString());
					statement.setString(10, gender);
					statement.setString(11, userStatus);
					resultSet=statement.executeQuery();
				}
				catch(Exception e)
				{
					System.out.println(e);
				}
				finally {
					statement.execute();
					statement.close();
				}
				AlertDialogue.Info("Registration is completed successfully!!");
				Main.showLoginPage();
		
			}
			else {
				AlertDialogue.Warning("Invalid Email", "'"+email+"'"+" is invalid email. Please enter a valid email");
			}
		}else {
			AlertDialogue.Warning("Empty field warning","Full name, Email, Password can't be empty");
		}
	}
	
	@FXML
	public void goExit(ActionEvent event) {
		System.exit(0);
	}
	
	public void initialize(){
		Department.setPromptText("Select your Department");
		Department.setItems(departmentlist);
	}
}
