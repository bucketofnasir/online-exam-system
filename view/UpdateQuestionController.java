package application.view;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import javafx.scene.control.TextField;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import application.AlertDialogue;
import application.Main;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;

public class UpdateQuestionController implements Initializable{
	Connection connection=DBConnection.DBConnector();
	ObservableList<QuestionLoader> data=FXCollections.observableArrayList();
	FilteredList<QuestionLoader> filteredData=new FilteredList<>(data,e->true);
	
	PreparedStatement statement=null;
	ResultSet rs=null;
	@FXML
	private TextField Question;
	@FXML
	private TextField option1;
	@FXML
	private TextField option2;
	@FXML
	private TextField option3;
	@FXML
	private TextField option4;
	@FXML
	private TextField Answer;
	@FXML
	private TextField Description;
	@FXML
	private Button UpdateBatton;

	static String qustionName=QuestionsController.questionName ;
	// Event Listener on Button[#UpdateBatton].onAction
	@FXML
	public void goUpdate(ActionEvent event) {
		try
		{
			String query="update Questions set question=?, option1=?,option2=?, option3=?, option4=?, Answer=?, Description=? where question='"+qustionName+"'";

			try
			{
				statement=connection.prepareStatement(query);
				statement.setString(1, Question.getText());
				statement.setString(2, option1.getText());
				statement.setString(3, option2.getText());
				statement.setString(4, option3.getText());
				statement.setString(5, option4.getText());
				statement.setString(6, Answer.getText());
				statement.setString(7, Description.getText());
				
				statement.execute();
				statement.close();
				AlertDialogue.showConfirmationAlertBox("question '"+Question.getText()+"' Updated Successfully!");	
			}
			catch(SQLException e)
			{
				System.out.println(e);
			}
			AlertDialogue.closeDialogStage(UpdateBatton);
			Main.showQuestions();
		}
		catch (Exception e)
		{
			System.err.println(e);
		}
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		Question.setText(QuestionsController.questionName);
		option1.setText(QuestionsController.option1);
		option2.setText(QuestionsController.option2);
		option3.setText(QuestionsController.option3);
		option4.setText(QuestionsController.option4);
		Answer.setText(QuestionsController.Answer);
		Description.setText(QuestionsController.Description);
		
	}
}
