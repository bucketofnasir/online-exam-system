package application.view;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import javafx.scene.control.TextField;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import application.AlertDialogue;
import application.Main;
import javafx.event.ActionEvent;

import javafx.scene.control.ComboBox;

import javafx.scene.control.DatePicker;

public class UpdateFacultyProfileController implements Initializable{
	
	/********************************************/
	/*******database connection properties*******/
	/********************************************/
	Connection connection=DBConnection.DBConnector();
	PreparedStatement statement=null;
	ResultSet rs=null;
	
	/**************************************/
	/***************Field properties*******/
	/**************************************/
	@FXML
	private TextField NameField;
	@FXML
	private TextField EmailField;
	@FXML
	private TextField IDField;
	@FXML
	private TextField PasswordField;
	@FXML
	private ComboBox<String> Department;
	@FXML
	private ComboBox<String> Gender;
	@FXML
	private TextField AddressField;
	@FXML
	private DatePicker BithDate;
	@FXML
	private TextField PhoneNumberField;
	@FXML
	private Button UpdateBatton;

	// Event Listener on Button[#UpdateBatton].onAction
	@FXML
	public void goUpdate(ActionEvent event) {
		try
		{
			String department= Department.getValue();
			String gender=Gender.getValue();
			String dateofbirth=BithDate.getValue().toString();
			String query="update UserInfo set Name=?, Email=?, ID=?, Password=?, Department=?, Gender=?, Address=?, BirthDate=?, PhoneNumber=? where Email='"+FacultyHomePageController.facultyEmail+"'";
			try
			{
				statement=connection.prepareStatement(query);
				statement.setString(1, NameField.getText());
				statement.setString(2, EmailField.getText());
				statement.setString(3, IDField.getText());
				statement.setString(4, PasswordField.getText());
				statement.setString(5, department);
				statement.setString(6, gender);
				statement.setString(7, AddressField.getText());
				statement.setString(8, dateofbirth);
				statement.setString(9, PhoneNumberField.getText());
				
				statement.executeUpdate();
				statement.close();
				AlertDialogue.showConfirmationAlertBox(EmailField.getText()+" Updated Successfully!");	
			}
			catch(SQLException e)
			{
				System.out.println(e);
			}
			AlertDialogue.closeDialogStage(UpdateBatton);
			Main.showFacultyHomePage();
		}
		catch (Exception e)
		{
			System.err.println(e);
		}
	}

	@Override
	public void initialize(URL args0, ResourceBundle args1) {
		 
		NameField.setText(FacultyHomePageController.name);
		EmailField.setText(FacultyHomePageController.facultyEmail);
		IDField.setText(FacultyHomePageController.ID);
		PasswordField.setText(FacultyHomePageController.password);
		AddressField.setText(FacultyHomePageController.address);
		PhoneNumberField.setText(FacultyHomePageController.phonenumber);
		Department.setPromptText(FacultyHomePageController.departent);
		Gender.setPromptText(FacultyHomePageController.gender);
		
		
	}
}
