package application.view;

public class ResultModel {
	String subject;
	String correctAnwer;
	String incorrectAnswer;
	String resultStatus;
	String remark;
	String id;
	
	public ResultModel(String subject, String correctAnwer, String incorrectAnswer, String resultStatus,
			String remark) {
		super();
		this.subject = subject;
		this.correctAnwer = correctAnwer;
		this.incorrectAnswer = incorrectAnswer;
		this.resultStatus = resultStatus;
		this.remark = remark;
	}
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getResultStatus() {
		return resultStatus;
	}
	public void setResultStatus(String resultStatus) {
		this.resultStatus = resultStatus;
	}
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getCorrectAnwer() {
		return correctAnwer;
	}
	public void setCorrectAnwer(String correctAnwer) {
		this.correctAnwer = correctAnwer;
	}
	public String getIncorrectAnswer() {
		return incorrectAnswer;
	}
	public void setIncorrectAnswer(String incorrectAnswer) {
		this.incorrectAnswer = incorrectAnswer;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
}
