package application.view;

public class SubjectLoader {
	String AllSubject;
	String TotalQuestion;

	public SubjectLoader(String subject, String totalQuestion) {
		this.AllSubject= subject;
		this.TotalQuestion= totalQuestion;
	}

	public String getAllSubject() {
		return AllSubject;
	}

	public void setAllSubject(String subject) {
		AllSubject = subject;
	}
	public String getTotalQuestion() {
		return TotalQuestion;
	}

	public void setTotalQuestion(String totalQuestion) {
		TotalQuestion = totalQuestion;
	}
	
}
