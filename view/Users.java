package application.view;

public class Users {
	private String Name;
	private String Email;
	private String ID;
	private String Password;
	private String Department;
	private String Gender;
	private String BirthDate;
	private String PhoneNumber;
	private String Status;
	private String UserRole;
	//Constructor for receiving data from database
	public Users(String name, String email, String iD, String passWord, String department, String gender,String DateofBirth, String number, String status, String UserRole) {
		super();
		Name = name;
		Email = email;
		ID = iD;
		Password = passWord;
		Department = department;
		Gender = gender;
		BirthDate = DateofBirth;
		PhoneNumber=number;
		Status=status;
		this.UserRole=UserRole;
	}
	
	//Gatter setter for name
	public String getName() {
		return Name;
	}
	public void setName(String name){
		this.Name=name;
	}

	//Gatter setter for Email
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email){
		this.Email=email;
	}
	

	//Gatter setter for ID
	public String getID() {
		return ID;
	}
	public void setID(String id){
		this.ID=id;
	}
	

	//Gatter setter for Password
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password){
		this.Password=password;
	}
	

	//Gatter setter for Department
	public String getDepartment() {
		return Department;
	}
	public void setDepartment(String department){
		this.Department=department;
	}
	

	//Gatter setter for Gender
	public String getGender() {
		return Gender;
	}
	public void setGender(String gender){
		this.Gender=gender;
	}
	

	//Gatter setter for Date of birth
	public String getBirthDate() {
		return BirthDate;
	}
	public void setBirthDate(String birthdate){
		this.BirthDate=birthdate;
	}
	//Gatter setter for Date of birth
	public String getPhoneNumber() {
		return PhoneNumber;
	}
	public void setPhoneNumber(String number){
		this.PhoneNumber=number;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status){
		this.Status=status;
	}

	public String getUserRole() {
		return UserRole;
	}

	public void setUserRole(String userRole) {
		UserRole = userRole;
	}
}
