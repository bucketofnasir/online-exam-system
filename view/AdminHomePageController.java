package application.view;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import java.util.function.Predicate;

import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import java.io.IOException;
import java.net.URL;

import application.AlertDialogue;
import application.Main;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class AdminHomePageController implements Initializable{
	/***************************************/
	/**********DB Connection Properties*****/
	/***************************************/
	Connection connection= DBConnection.DBConnector();
	ObservableList<Users> Data= FXCollections.observableArrayList();
	java.sql.PreparedStatement preparedStatement= null;
	ResultSet rs=null;
	
	/***************************************/
	/**********Search User Properties*******/
	/***************************************/
	FilteredList<Users> filteredData=new FilteredList<>(Data,e->true);
	
	
	
	/**************************************/
	/******GUI properties******************/
	/**************************************/
	@FXML
	TableView<Users> table;
	@FXML
	private TableColumn<?, ?> nameCol;
	@FXML
	private TableColumn<?, ?> emailCol;
	@FXML
	private TableColumn<?, ?> IDCol;
	@FXML
	private TableColumn<?, ?> passwordCol;
	@FXML
	private TableColumn<?, ?> departmentCol;
	@FXML
	private TableColumn<?, ?> genderCol;
	@FXML
	private TableColumn<?, ?> birthdayCol;
	@FXML
	private TableColumn<?, ?> numberCol;
	@FXML
	private TableColumn<?, ?>StatusCol;
	@FXML
	private MenuItem CloseButton;
	@FXML
	private MenuItem EditStudent;
	@FXML
	private MenuItem EditFaculty;
	@FXML
	private MenuItem AddStudent;
	@FXML
	private MenuItem AddFaculty;
	@FXML
	private MenuItem EnableStudent;
	@FXML
	private MenuItem EnableFaculty;
	@FXML
	private MenuItem DeleteQuestion;
	@FXML
	private MenuItem UpdateResult;
	@FXML
	private MenuItem DeleteResult;
	@FXML
	private MenuItem About;
	@FXML
	private Button FacultyButton;
	@FXML
	private Button StudentButton;
	@FXML
	private Button SubjectButton;
	@FXML
	private TextField SearchBox;
	@FXML
	private Button SearchButton;
	@FXML
	private Button LogOutButton;
	@FXML
	private Button AllMemberButton;
	@FXML
	private MenuItem DeletePerson;
	
	
	/*******************************************/
	/*************initializing home page********/
	/*******************************************/
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		nameCol.setCellValueFactory(new PropertyValueFactory<>("Name"));
		emailCol.setCellValueFactory(new PropertyValueFactory<>("Email"));
		IDCol.setCellValueFactory(new PropertyValueFactory<>("ID"));
		passwordCol.setCellValueFactory(new PropertyValueFactory<>("Password"));
		departmentCol.setCellValueFactory(new PropertyValueFactory<>("Department"));
		genderCol.setCellValueFactory(new PropertyValueFactory<>("Gender"));
		birthdayCol.setCellValueFactory(new PropertyValueFactory<>("BirthDate"));
		numberCol.setCellValueFactory(new PropertyValueFactory<>("PhoneNumber"));
		StatusCol.setCellValueFactory(new PropertyValueFactory<>("Status"));
		
		loadDatabaseData("");
	}
	
	
	/*****************************************/
	/**********Information loading from DB****/
	/*****************************************/
	public void loadDatabaseData(String uroll){
		
		String query="SELECT * FROM UserInfo WHERE UserRoll='"+uroll+"'";
		try {
			Data.clear();
			preparedStatement=connection.prepareStatement(query);
			rs=preparedStatement.executeQuery();
			
			while (rs.next()) {
				Data.add(new Users(
						rs.getString("Name"), 
						rs.getString("Email"),
						rs.getString("ID"), 
						rs.getString("Password"), 
						rs.getString("Department"), 
						rs.getString("Gender"), 
						rs.getString("BirthDate"),
						rs.getString("PhoneNumber"),
						rs.getString("Status"),
						rs.getString("UserRoll")
						));
				table.setItems(Data);
			}
			preparedStatement.close();
			rs.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	
public void loadallData(){
		
		String query="SELECT * FROM UserInfo";
		try {
			Data.clear();
			preparedStatement=connection.prepareStatement(query);
			rs=preparedStatement.executeQuery();
			
			while (rs.next()) {
				Data.add(new Users(
						rs.getString("Name"), 
						rs.getString("Email"),
						rs.getString("ID"), 
						rs.getString("Password"), 
						rs.getString("Department"), 
						rs.getString("Gender"), 
						rs.getString("BirthDate"),
						rs.getString("PhoneNumber"),
						rs.getString("Status"),
						rs.getString("UserRoll")
						));
				table.setItems(Data);
			}
			preparedStatement.close();
			rs.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	@FXML
	public void goClose(ActionEvent event) {
		System.exit(0);
	}
	// Event Listener on MenuItem[#EditStudent].onAction
	@FXML
	public void goStudentDisable(ActionEvent event) {
		ButtonType buttonType=AlertDialogue.showConfirmationAlertBox("Do you want to disable?");
		if (buttonType != ButtonType.OK)
			return;
		try
		{
			Users Student = (Users) table.getSelectionModel().getSelectedItem();
			if (Student!=null) {
				if (Student.getUserRole()=="Student") {
					String query = "update UserInfo set Status=? where Email='"+Student.getEmail()+"'";
					preparedStatement = connection.prepareStatement(query);
					preparedStatement.setString(1, "Disable");
					preparedStatement.execute();

					preparedStatement.close();
					rs.close();
					Main.showAdminHomePage();
					AlertDialogue.Info(Student.getEmail()+"is disabled successfully");
				}
				else {
					AlertDialogue.Warning("Invalide selection", "Selected person is not student");
				}
			}
			else {
				AlertDialogue.Error("Disable error","No Student is selected!");
			}
		}
		catch (Exception e)
		{
			System.err.println(e);
		}
	}
	// Event Listener on MenuItem[#EditFaculty].onAction
	@FXML
	public void goFacultyDisable(ActionEvent event) {
		// TODO Autogenerated
		Users Faculty = (Users) table.getSelectionModel().getSelectedItem();
		ButtonType buttonType=AlertDialogue.showConfirmationAlertBox("Do you want to disable this faculty?");
		if (buttonType != ButtonType.OK)
			return;
		try
		{
			if (Faculty!=null) {
					String query = "update UserInfo set Status=? where Email='"+Faculty.getEmail()+"'";
					preparedStatement = connection.prepareStatement(query);
					preparedStatement.setString(1, "Disable");
					preparedStatement.execute();

					preparedStatement.close();
					Main.showAdminHomePage();
					AlertDialogue.Info(Faculty.getName()+"is disabled successfully");
				
			}
			else {
				AlertDialogue.Error("No Selection","No faculty is selected!");
			}
		}
		catch (Exception e)
		{
			System.err.println(e);
		}
	}

	// Event Listener on MenuItem[#AddQuestion].onAction
	@FXML
	public void goEnableStudent(ActionEvent event) {
		ButtonType buttonType=AlertDialogue.showConfirmationAlertBox("Do you want to enable?");
		if (buttonType != ButtonType.OK)
			return;
		try
		{
			Users Student = (Users) table.getSelectionModel().getSelectedItem();
			if (Student!=null) {
				if (Student.getStatus()=="Student") {
					String query = "update UserInfo set Status=? where Email='"+Student.getEmail()+"'";
					preparedStatement = connection.prepareStatement(query);
					preparedStatement.setString(1, "Enable");
					preparedStatement.execute();

					preparedStatement.close();
					rs.close();
					Main.showAdminHomePage();
					AlertDialogue.Info(Student.getName()+"is Enabled successfully");
				}
				else{
					AlertDialogue.Error("Selectiion error", "Selected person is not Student");
				}
			}
			else {
				AlertDialogue.Error("Enable error","No student is selected!");
			}
		}
		catch (Exception e)
		{
			System.err.println(e);
		}
		
	}
	@FXML
	public void goEnableFaculty(ActionEvent event) {
		Users Faculty = (Users) table.getSelectionModel().getSelectedItem();
		ButtonType buttonType=AlertDialogue.showConfirmationAlertBox("Do you want to enable?");
		if (buttonType != ButtonType.OK)
			return;
		try
		{
			
			if (Faculty!=null) {
				String query = "update UserInfo set Status=? where Email='"+Faculty.getEmail()+"'";
				preparedStatement = connection.prepareStatement(query);
				preparedStatement.setString(1, "Enable");
				preparedStatement.execute();

				preparedStatement.close();
				rs.close();
				Main.showAdminHomePage();
				AlertDialogue.Info(Faculty.getName()+"is Enabled successfully");
			}
			else {
				AlertDialogue.Error("Enable error","Faculty can't be Enabled!");
			}
		}
		catch (Exception e)
		{
			System.err.println(e);
		}
	
	}
	// Event Listener on MenuItem[#DeleteQuestion].onAction
	@FXML
	public void goDeleteQuestion(ActionEvent event) {
	}
	// Event Listener on MenuItem[#UpdateResult].onAction
	@FXML
	public void goUpdateResult(ActionEvent event) {
		// TODO Autogenerated
	}
	// Event Listener on MenuItem[#DeleteResult].onAction
	@FXML
	public void goDeleteResult(ActionEvent event) {
		// TODO Autogenerated
	}
	// Event Listener on MenuItem[#About].onAction
	@FXML
	public void goAbout(ActionEvent event) {
		// TODO Autogenerated
	}
	@FXML
	public void goFaculty(ActionEvent event) {
		loadDatabaseData("Faculty");
	}
	@FXML
	public void goStudent(ActionEvent event) {
		loadDatabaseData("Student");
		
	}
	@FXML
	public void goAllMember(){
		loadallData();
	}
	// Event Listener on Button[#SubjectButton].onAction
	@FXML
	public void goSubject(ActionEvent event) throws IOException {
		// TODO Autogenerated
		QuestionsController.Author="Admin";
		Main.showQuestions();
		
	}
	@FXML
	public void goDeletePerson(){

		ButtonType buttonType=AlertDialogue.showConfirmationAlertBox("Do you want to delete?");
		if (buttonType != ButtonType.OK)
			return;
		String email;
		try
		{
			Users person = (Users) table.getSelectionModel().getSelectedItem();
			if (person!=null) {
					String query = "delete from UserInfo where Email=?";

					preparedStatement = connection.prepareStatement(query);
					preparedStatement.setString(1, person.getEmail());
					email = person.getEmail();
					preparedStatement.executeUpdate();

					preparedStatement.close();
					rs.close();
					Main.showAdminHomePage();
					AlertDialogue.Info("Student '" + email + "' Deleted Successfully!");
			}
			else {
				AlertDialogue.Error("Empty Selection","No Person is selected!");
			}
		}
		catch (Exception e)
		{
			System.err.println(e);
		}
	}
	
	// Event Listener on Button[#SearchButton].onAction
	@FXML
	public void goSearch(ActionEvent event) {
		SearchBox.textProperty().addListener((observableValue,oldValue,newValue)->{
			filteredData.setPredicate((Predicate<? super Users>)user->{
				if(newValue==null||newValue.isEmpty()){
					return true;
				}
				String lowerCaseFilter=newValue.toLowerCase();
				if(user.getName().toLowerCase().contains(lowerCaseFilter)){
					return true;
				}
				else if(user.getEmail().toLowerCase().contains(lowerCaseFilter)){
					return true;
				}
				else if(user.getID().toLowerCase().contains(lowerCaseFilter)){
					return true;
				}
				return false;
			});
		});
		SortedList<Users> sortedData=new SortedList<>(filteredData);
		sortedData.comparatorProperty().bind(table.comparatorProperty());
		table.setItems(sortedData);
	}
	// Event Listener on Button[#LogOutButton].onAction
	@FXML
	public void goLogout(ActionEvent event) throws IOException {
		Main.showLoginPage();
	}
	
}
