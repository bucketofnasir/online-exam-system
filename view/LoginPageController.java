package application.view;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.io.IOException;
import java.sql.SQLException;

import application.AlertDialogue;
import application.Main;
import application.Validation;
import javafx.event.ActionEvent;

import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;

public class LoginPageController {
	LoginModel loginModel= new LoginModel();
	
	@FXML
	private TextField AdminUserName;
	@FXML
	PasswordField AdminPassword;
	@FXML
	private Button AdminLoginButton;
	@FXML
	private Button AdminClear;
	@FXML
	private Button AdminCancel;
	@FXML
	private TextField TeacherEmail;
	@FXML
	private PasswordField TeacherPassword;
	@FXML
	private Button TeacherLoginButton;
	@FXML
	private Button TeacherClear;
	@FXML
	private Button TeacherCancel;
	@FXML
	private Button TeacherRegistrationButton;
	@FXML
	private TextField StudentID;
	@FXML
	private PasswordField StudentPassword;
	@FXML
	private Button StudentLoginButton;
	@FXML
	private Button StudentClear;
	@FXML
	private Button StudentCancel;
	@FXML
	private Button StudentRegistrationButton;
	@FXML
	private MenuItem Exit;
	@FXML
	private MenuItem Documentation;
	@FXML
	private MenuItem About;
	@FXML
	private Text ErrorMessage;
	@FXML
	private Text StudentErrorMessage;
	@FXML
	private Text TeacherErrorMessage;
	
	

	@FXML
	public void goAdminLogin() throws IOException, SQLException {
		String admin="Admin";
		if(LoginModel.isLoginValidForAdmin((AdminUserName.getText()), (AdminPassword.getText()), admin)==true)
		{
			Main.showAdminHomePage();
		}
		else
		{
			AlertDialogue.Error("Logging Error","Invalid Username and Password!");
		}
		
	}
	

	@FXML
	public void goClear(ActionEvent event) {
		TeacherEmail.setText("");
		TeacherPassword.setText(null);
		StudentID.setText("");
		StudentPassword.setText(null);
		AdminUserName.setText(null);
		AdminPassword.setText(null);
	}

	@FXML
	public void goCancel(ActionEvent event) {
		System.exit(0);
	}

	@FXML
	public void goTeacherLogin(ActionEvent event) throws IOException, SQLException {
		String faculty="Faculty";
		String Status="Enable";
		if (TeacherEmail.getText().isEmpty()==false) {
			if (Validation.EmailValidation(TeacherEmail.getText())) {
				if(LoginModel.isLoginValidForFaculty((TeacherEmail.getText()), (TeacherPassword.getText()), faculty, Status)==true)
				{
					FacultyHomePageController.facultyEmail=TeacherEmail.getText();
					Main.showFacultyHomePage();
				}
				else{
					AlertDialogue.Error("Logging Error","invalid Username and Password!");
				}
				
			}
			else{
				AlertDialogue.Warning("Invalid Email warning","Invalid email");
			}
		}
		else {
			AlertDialogue.Warning("Empty field", "Email or password is empty");
		}
		
	}

	@FXML
	public void goTeacherRegistration(ActionEvent event) throws IOException {
		RegistrationPageController.userRole="Faculty";
		Main.showRegistrationPage();
	}

	@FXML
	public void goStudentLogin(ActionEvent event) throws IOException, SQLException {
		String student="Student";
		String status="Enable";
		if (StudentID.getText().isEmpty()==false) {
			if (Validation.IDValidation(StudentID.getText())==true) {
				if(LoginModel.isLoginValidForStudent((StudentID.getText()), (StudentPassword.getText()), student, status)==true)
				{
					StudentHomePageController.ID=StudentID.getText();
					Main.showStudentHomePage();
				}
				else
				{
					AlertDialogue.Error("Logging Error","Invalid Username and Password! or account is disable");
				}
			}
			else {
				AlertDialogue.Warning("Invalid ID warning","'"+StudentID.getText()+"'"+" is an invalid ID number");
			}
		}
		else {
			AlertDialogue.Warning("Empty ID field", "Enter your ID");
		}
	}
	
	@FXML
	public void goStudentRegistration(ActionEvent event) throws IOException {
		RegistrationPageController.userRole="Student";
		Main.showRegistrationPage();
	}
	
	@FXML
	public void goExit(ActionEvent event) {
		System.exit(0);
	}
	
	@FXML
	public void goDocumentation(ActionEvent event) {
		// TODO Autogenerated
	}

	@FXML
	public void goAbout(ActionEvent event) {
		
	}
	
	
}
