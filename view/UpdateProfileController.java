package application.view;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import javafx.scene.control.TextField;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import application.AlertDialogue;
import application.Main;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

import javafx.scene.control.ComboBox;

import javafx.scene.control.DatePicker;

public class UpdateProfileController implements Initializable{
	
	/********************************************/
	/*******database connection properties*******/
	/********************************************/
	Connection connection=DBConnection.DBConnector();
	PreparedStatement statement=null;
	ResultSet rs=null;
	
	/**************************************/
	/***************Field properties*******/
	/**************************************/
	@FXML
	private TextField NameField;
	@FXML
	private TextField EmailField;
	@FXML
	private TextField IDField;
	@FXML
	private TextField PasswordField;
	@FXML
	private ComboBox<String> Department;
	@FXML
	private ComboBox<String> Gender;
	@FXML
	private TextField AddressField;
	@FXML
	private DatePicker BithDate;
	@FXML
	private TextField PhoneNumberField;
	@FXML
	private Button UpdateBatton;
	
	ObservableList<String> SubjectList= FXCollections.observableArrayList(
			loadSubject()
			);
	ObservableList<String> gender= FXCollections.observableArrayList(
			"Male", "Female"
			);

	// Event Listener on Button[#UpdateBatton].onAction
	@FXML
	public void goUpdate(ActionEvent event) {
		try
		{
			String department= Department.getValue();
			String gender=Gender.getValue();
			String dateofbirth=BithDate.getValue().toString();
			String query="update UserInfo set Name=?, Email=?, ID=?, Password=?, Department=?, Gender=?, Address=?, BirthDate=?, PhoneNumber=? where ID='"+StudentHomePageController.ID+"'";

			try
			{
				statement=connection.prepareStatement(query);
				statement.setString(1, NameField.getText());
				statement.setString(2, EmailField.getText());
				statement.setString(3, IDField.getText());
				statement.setString(4, PasswordField.getText());
				statement.setString(5, department);
				statement.setString(6, gender);
				statement.setString(7, AddressField.getText());
				statement.setString(8, dateofbirth);
				statement.setString(9, PhoneNumberField.getText());
				
				statement.executeUpdate();
				statement.close();
				AlertDialogue.showConfirmationAlertBox(IDField.getText()+" Updated Successfully!");	
			}
			catch(SQLException e)
			{
				System.out.println(e);
			}
			AlertDialogue.closeDialogStage(UpdateBatton);
			Main.showStudentHomePage();
		}
		catch (Exception e)
		{
			System.err.println(e);
		}
	}

	@Override
	public void initialize(URL args0, ResourceBundle args1) {
		Department.setItems(SubjectList);
		Gender.setItems(gender);
		Gender.setPromptText("Select Gender");
		NameField.setText(StudentHomePageController.name);
		EmailField.setText(StudentHomePageController.email);
		IDField.setText(StudentHomePageController.ID);
		PasswordField.setText(StudentHomePageController.password);
		AddressField.setText(StudentHomePageController.address);
		PhoneNumberField.setText(StudentHomePageController.phonenumber);
		Department.setPromptText("Select Department");
		Gender.setPromptText(StudentHomePageController.gender);
		
		
	}
	public ArrayList<String> loadSubject(){
		ArrayList<String>list = new ArrayList<>();
		String query="SELECT * from Subject";
		try {
			statement=connection.prepareStatement(query);
			rs=statement.executeQuery();
			while (rs.next()) {
				list.add(rs.getString("AllSubject"));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
}
