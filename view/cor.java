package application.view;

public class cor {
	public String corAns, Status, remark;
	public int cor, inc;

	public cor() {
		super();
	}

	public void setCorAns(String corAns) {
		this.corAns = corAns;
	}
	public String getCorAns() {
		return this.corAns;
	}
	public void setcor(int i){
		this.cor=i;
	}
	public int getcor(){
		return this.cor;
	}
	public void setinc(int i){
		this.inc=i;
	}
	public int getinc(){
		return this.inc;
	}
	public void setStatus(String s){
		this.Status=s;
	}
	public String getStatus(){
		return this.Status;
	}

	public void setremark(String s){
		this.remark=s;
	}
	public String getremark(){
		return this.remark;
	}
	
}
