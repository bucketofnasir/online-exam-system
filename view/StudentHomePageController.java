package application.view;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import application.AlertDialogue;
import application.Main;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;

import javafx.scene.control.MenuItem;

import javafx.scene.control.Label;

import javafx.scene.control.ComboBox;

import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.TableColumn;

public class StudentHomePageController implements Initializable{
	/****************************************************/
	/****************Database properties*****************/
	/****************************************************/
	Connection connection=DBConnection.DBConnector();
	PreparedStatement statement=null;
	PreparedStatement statement2=null;
	ResultSet rs=null;
	boolean result;
	boolean result2;
	
	
	/****************************************************/
	/*******Load Result from Database*******************/
	/***************************************************/
	ObservableList<ResultModel> Data= FXCollections.observableArrayList();
	FilteredList<ResultModel> filteredData=new FilteredList<>(Data,e->true);
	
	
	
	
	
	/*****************************************************/
	/**All label, button, Combobox and menuitem variable**/
	/*****************************************************/
	@FXML
	private Button logOutbtn;
	@FXML
	private MenuItem Close;
	@FXML
	private MenuItem updateProfile;
	@FXML
	private MenuItem deleteAccount;
	@FXML
	private MenuItem About;
	@FXML
	private Label LoggedAs;
	@FXML
	private Label NameLabel;
	@FXML
	private Label IDLabel;
	@FXML
	private Label EmailLabel;
	@FXML
	private Label DepartmentName;
	@FXML
	private Label PositionLabel;
	@FXML
	private ComboBox<String> ExamSubjectCombobox;
	@FXML
	private Button goExam;
	
	/****************************************************/
	/***********Table view and column variables**********/
	/****************************************************/
	@FXML
	private TableView<ResultModel> ResultTable;
	@FXML
	private TableColumn<?, ?> SubjectCol;
	@FXML
	private TableColumn<?, ?> CorrectAnswerCol;
	@FXML
	private TableColumn<?, ?> IncorrectAnswerCol;
	@FXML
	private TableColumn<?, ?> ResultStatusCol;
	@FXML
	private TableColumn<?, ?> RemarkCol;
	
	
	
	
	
	
	/****************************/
	/******Custom Variables******/
	/****************************/
	public static String ID;
	public static String name;
	public static String email;
	public static String password;
	public static String departent;
	public static String gender;
	public static String address;
	public static String phonenumber;
	public static String dateofbirth;
	public static String userRole;
	
	ObservableList<String> SubjectList= FXCollections.observableArrayList(
			loadSubject()
			);

	
	
	/*********************************/
	/******All Methods Start**********/
	/*********************************/
	// Event Listener on Button[#logOutbtn].onAction
	@FXML
	public void goLogout(ActionEvent event) throws IOException {
		Main.showLoginPage();
	}
	// Event Listener on MenuItem[#Close].onAction
	@FXML
	public void goClose(ActionEvent event) {
		System.exit(0);
	}
	// Event Listener on MenuItem[#updateProfile].onAction
	@FXML
	public void goUpdateProfile(ActionEvent event) throws IOException {
		
		String query="SELECT * FROM UserInfo WHERE ID='"+ID+"'";
		try {
			statement=connection.prepareStatement(query);
			rs=statement.executeQuery();
			while(rs.next()){
				ID=rs.getString("ID");
				name=rs.getString("Name");
				email=rs.getString("Email");
				departent=rs.getString("Department");
				gender=rs.getString("Gender");
				dateofbirth=rs.getString("BirthDate");
				address=rs.getString("Address");
				phonenumber=rs.getString("PhoneNumber");
				password=rs.getString("Password");
				userRole=rs.getString("UserRoll");
			}
			Main.showUpdateProfile();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	@FXML
	public void goStartExam() throws IOException, SQLException{
		ExamController.subject=ExamSubjectCombobox.getValue();
			String s=ExamSubjectCombobox.getValue();
			if (s.isEmpty()==true) {
				AlertDialogue.Error("Empty Selection", "Select a subject!");
			}
			else {
				Main.showExam();
			}
	}
	// Event Listener on MenuItem[#deleteAccount].onAction
	@FXML
	public void goDeleteAccount(ActionEvent event) throws SQLException, IOException{
		String query="DELETE FROM UserInfo WHERE ID='"+ID+"'";
		String ResultDeleteQuery="DELETE FROM Result WHERE ID='"+ID+"'";
		try {
			statement=connection.prepareStatement(query);
			statement2=connection.prepareStatement(ResultDeleteQuery);
			result=statement.execute();
			result2=statement2.execute();
			if (result!=true&&result2!=true) {
				AlertDialogue.showConfirmationAlertBox("Account is deleted Successfully!");
			}
			else {
				AlertDialogue.Error("Deletion error", ID+" account can't be deleted!");
			}
		} catch (SQLException e) {
			System.err.println(e);
		}
		finally {
			statement.close();
			rs.close();
		}
		Main.showLoginPage();
	}
	
	@FXML
	public void goAbout(ActionEvent event) {
		// TODO Autogenerated
	}
	@Override
	public void initialize(URL args0, ResourceBundle args1) {
		ExamSubjectCombobox.setPromptText("Select a subject");
		ExamSubjectCombobox.setItems(SubjectList);
		loadResult();
		LoggedAs.setText("Now you are logged in as: "+ID);
		ExamSubjectCombobox.setPromptText("Select a subject");
		try {
			profileProperties();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		SubjectCol.setCellValueFactory(new PropertyValueFactory<>("Subject"));
		CorrectAnswerCol.setCellValueFactory(new PropertyValueFactory<>("correctAnwer"));
		IncorrectAnswerCol.setCellValueFactory(new PropertyValueFactory<>("incorrectAnswer"));
		ResultStatusCol.setCellValueFactory(new PropertyValueFactory<>("resultStatus"));
		RemarkCol.setCellValueFactory(new PropertyValueFactory<>("Remark"));
	}
	private void profileProperties() throws SQLException {
		String query="SELECT * FROM UserInfo WHERE ID='"+ID+"'";
		try {
			statement=connection.prepareStatement(query);
			rs=statement.executeQuery();
			while (rs.next()) {
				NameLabel.setText(rs.getString("Name"));
				IDLabel.setText(rs.getString("ID"));
				EmailLabel.setText(rs.getString("Email"));
				DepartmentName.setText(rs.getString("Department"));
				PositionLabel.setText(rs.getString("Phonenumber"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			statement.close();
			rs.close();
		}
		
	}
	
	public void loadResult(){
		String query="SELECT * FROM Result WHERE ID=?";
		try {
			Data.clear();
			statement=connection.prepareStatement(query);
			statement.setString(1, ID);
			rs=statement.executeQuery();
			
			while (rs.next()) {
				Data.add(new ResultModel(
						rs.getString("Subject"),
						rs.getString("Correct"),
						rs.getString("Incorrect"), 
						rs.getString("Status"), 
						rs.getString("Remark")
						));
				ResultTable.setItems(Data);
			}
			statement.close();
			rs.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		
	}
	public ArrayList<String> loadSubject(){
		ArrayList<String>list = new ArrayList<>();
		String query="SELECT * from Subject";
		try {
			statement=connection.prepareStatement(query);
			rs=statement.executeQuery();
			while (rs.next()) {
				list.add(rs.getString("AllSubject"));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
}
